var express = require('express');
require('dotenv').config()
var app = express();

app.get('/', function (request, response) {
  response.send(`The database connection URL is ${process.env.DATABASE_CONNECTION_URL} for the ${process.env.NODE_ENV} environment`);
});

app.listen(4010);
