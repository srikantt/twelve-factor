## ## Twelve Factors - binding

docker build -t hartree/binding:1.0 .

docker run -d -p 5009:5000 hartree/binding:1.0 .

### multiple instances can be started, for example

docker run -d -p 5010:5000 hartree/binding:1.0 .
