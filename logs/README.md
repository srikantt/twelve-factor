## Twelve Factors - logs

## Twelve Factors - Build, release, run

docker build -t hartree/logs:1.0 .

docker run -dp 3000:3000 logs

### use docker ps to find the logs container

docker ps

docker logs <container-id>

