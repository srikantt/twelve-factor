process.env.POSTGRES_USER='postgres'
process.env.POSTGRES_PASSWORD='postgrespassword'
process.env.POSTGRES_HOST='timescale'
process.env.POSTGRES_DB='postgres'
process.env.POSTGRES_PORT=5432
process.env.APPLICATION_NAME='hartree-graphql'
process.env.LISTEN_PORT=4000
process.env.LOG_LEVEL='info'
process.env.NODE_ENV='development'
