
import { ApolloServer, gql } from "apollo-server-express";
import { resolvers, TemperatureServiceImpl, typeDefs } from "../src/graphql/resolvers";
import { db, pool } from "../src/db";

export const RECENT_TEMPERATURES = gql`
query RecentTempQuery {
  recentTemperatures {
    fivesecinterval
    location
    maxtemp
  }
}
`;

export const ADD_TEMPERATURE = gql`
mutation AddTemperatureMutation($location: String!, $temperature: Float!) {
    addTemperature(location: $location, temperature: $temperature)
}
`

afterAll((done) => {
    pool.end();
    done();
});

test('recent temperature query', async () => {

    // create a test server to test against
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context: {
            temperatureService: new TemperatureServiceImpl(db)
        },
        rootValue: db
    });

    // run query against the server
    const result = await server.executeOperation({ query: RECENT_TEMPERATURES, variables: { }});
    expect(result.errors).toBeUndefined();
    expect(result.data?.recentTemperatures).toBeDefined();
    server.stop();
});

test('add temperature', async () => {
    // create a test server to test against
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context: {
            temperatureService: new TemperatureServiceImpl(db)
        },
        rootValue: db
    });

    // run mutation against the server
    const result = await server.executeOperation({ query: ADD_TEMPERATURE, variables: {location:"London", temperature: 20.0 }});
    expect(result.errors).toBeUndefined();
    console.log(JSON.stringify(result));
    expect(result.data?.addTemperature).toBe(true);
    server.stop()
});