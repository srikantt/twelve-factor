// import { readdirSync } from 'fs';
// import path from 'path';
import { GraphQLResolveInfo } from "graphql";
import { gql } from "apollo-server-express";
import { MergeInfo } from "graphql-tools";
import { APIResponse, DB, QueryConfig } from "../../types";
import { dateScalar } from "../../utility/dateUtils";
import { LOGGER } from "../../utility/logger";

interface AppContext {
  temperatureService: TemperatureService;
}

export const typeDefs = gql`
 scalar Date
  type Temperature {
    location: String 
    temperature:Float
    recordedat: String
  }
  
  type recentTemperature {
    fivesecinterval: Date
    location: String
    maxtemp: Float
  }
  
  type Mutation {
    addTemperature(location: String!, temperature:Float!): Boolean
  }
  
  type Query {
      allTemperatures(location: String): [ Temperature ] 
      recentTemperatures(location: String): [ recentTemperature ] 
  }
`;

export const resolvers = {
  Date: dateScalar,
  Query: {
    allTemperatures: (
        parent: undefined,
        args: TemperatureArgs,
        context: AppContext,
        info: GraphQLResolveInfo & { mergeInfo: MergeInfo },
    ) => {
      return context.temperatureService.allTemperatures(args.location);
    },


    recentTemperatures: (
        parent: undefined,
        args: TemperatureArgs,
        context: AppContext,
        info: GraphQLResolveInfo & { mergeInfo: MergeInfo },
    ) => {
      return context.temperatureService.recentTemperatures(args.location);
    },
  },
  Mutation: {
    addTemperature: (
        parent: undefined,
        args: AddTemperatureArgs,
        context: AppContext,
        info: GraphQLResolveInfo & { mergeInfo: MergeInfo },
    ) => {
      return context.temperatureService.addTemperature(args.location, args.temperature);
    },
  },
};





export interface TemperatureService  {
  addTemperature(location: string, temperature: number): boolean;
  allTemperatures(location: string): Temperature[];
  recentTemperatures(location: string): RecentTemperature[];
}
export class TemperatureServiceImpl {
  db: DB;

  constructor(dbVar: DB) {
    this.db = dbVar;
  }


  async addTemperature(location: string, temperature: number) {
    try {
      LOGGER.info(`addTemperature request`);
      const query: QueryConfig = {
        text: "INSERT INTO hartree.temperature(location, temperature) VALUES($1, $2);",
        values: [location, temperature.toString()]
      };
      const result = await this.db.query(query);
      return true;
    } catch (error) {
      LOGGER.error(`addTemperature failed with ${error} `);
      return false;
    }
    return true;
  }

  async allTemperatures(location?: string) {

    const query: QueryConfig = {text: "SELECT location, temperature, recordedat FROM hartree.temperature"};
    LOGGER.info(`allTemperatures request`);

    const result = await this.db.query(query);

    const response: APIResponse<Temperature[]> = {
      status: "fetching",
      data: []
    };

    try {
      if (result.rowCount > 0) {
        const data = result.rows.map((item: Temperature) => {
          return {
            location: item.location,
            temperature: item.temperature,
            recordedat: item.recordedat
          };
        });
        response.status = "success";
        response.data = data;
      }
    } catch (error) {
      response.status = "error";
      LOGGER.error(`addTemperature failed with ${error} `);
    }
    return response.data;
  }

  async recentTemperatures(location?: string) {
    LOGGER.info(`recentTemperatures request`);
    const query: QueryConfig = {text: "SELECT location, maxtemp, fivesecinterval  FROM hartree.lasttwentymintemperatures"};


    const result = await this.db.query(query);

    const response: APIResponse<Temperature[]> = {
      status: "fetching",
      data: []
    };
    try {
      if (result.rowCount > 0) {
        const data = result.rows.map((item: RecentTemperature) => {
          return {
            location: item.location,
            fivesecinterval: item.fivesecinterval,
            maxtemp: item.maxtemp
          };
        });
        response.status = "success";
        response.data = data;
      }
    } catch (error) {
      response.status = "error";
      LOGGER.error(`recentTemperatures failed with ${error} `);
    }
    return response.data;
  }
}


