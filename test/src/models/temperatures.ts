
interface AddTemperatureArgs {
    location: string;
    temperature: number;
}

interface TemperatureArgs {
    location: string;
}

interface Temperature {
    location: string;
    temperature: number;
    recordedat: string;
}

interface RecentTemperature {
    fivesecinterval: number;
    location: string;
    maxtemp: number;
}

