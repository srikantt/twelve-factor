/*
 * Define the GraphQL Scalar type for Date.
 *
 */

const { GraphQLScalarType, Kind } = require("graphql");

export const dateScalar = new GraphQLScalarType({
    name: "Date",
    description: "Date custom scalar type",
    serialize(value) {
        return value.getTime(); // Convert outgoing Date to integer for JSON
    },
    parseValue(value) {
        return new Date(value); // Convert incoming integer to Date
    },
    parseLiteral(ast) {
        if (ast.kind === Kind.INT) {
            return new Date(parseInt(ast.value, 10)); // Convert AST string to integer and then to Date
        }
        return undefined; // Invalid value (not an integer)
    },
});