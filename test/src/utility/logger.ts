/*
 * Setup the LOGGER used by the GraphQL Server
 *
 */
import os from "os";
import winston from "winston";
import { APPLICATION_NAME } from "./constants";

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4,
};

const colors = {
    error: "red",
    warn: "yellow",
    info: "green",
    http: "magenta",
    debug: "blue"
};

winston.addColors(colors);

const format = winston.format.combine(
    winston.format.label({ label: "graphql" }),
    winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }),
    winston.format.colorize({ all: true }),
    winston.format.printf(
        (info) => `${info.timestamp} ${info.level}: ${APPLICATION_NAME} on ${os.hostname()}: ${info.message}`,
    ),
);

const transports = [
    new winston.transports.Console()
];

export const LOGGER = winston.createLogger({
    level: "info",
    levels,
    format,
    transports,
});


