/*
 * Create and export configuration variables used by the GraphQL Server
 *
 */
import os from "os";
import { APPLICATION_NAME } from "./constants";
import { readPathName } from "./secrets";

interface DatabaseConfig {
    readonly user: string;
    readonly password: string;
    readonly database: string;
    readonly host: string;
    readonly port: number;
}

interface EnvironmentConfig {
    readonly hostName: string;
    readonly listenPort: number;
    readonly env: string;
    readonly logLevel: string;
    readonly timeseriesDatabase: DatabaseConfig;
    readonly applicationName: string;
}

export const environmentConfig: EnvironmentConfig = {
    hostName: os.hostname(),
    listenPort:  typeof process.env.LISTEN_PORT === "number" ? process.env.LISTEN_PORT : 4000,
    env: process.env.NODE_ENV ? process.env.NODE_ENV : "production",
    logLevel: process.env.LOG_LEVEL ? process.env.LOG_LEVEL : "warn",
    timeseriesDatabase: {
        host: process.env.POSTGRES_HOST,
        database: process.env.POSTGRES_DB ? process.env.POSTGRES_DB : "postgres",
        password: process.env.NODE_ENV === "production" ? readPathName(process.env.TIMELINE_SECRET_PATH) : process.env.POSTGRES_PASSWORD as string,
        user: process.env.POSTGRES_USER ? process.env.POSTGRES_USER : "postgres",
        port: typeof process.env.POSTGRES_PORT === "number" ? process.env.POSTGRES_PORT : 5432
    },
    applicationName: APPLICATION_NAME
};
