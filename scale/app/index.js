const http = require('http');
const server = process.env.SERVER || 'default';
const port = process.env.PORT || 6000;

const httpServer = http.createServer(function (request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.end(`Hi, This is a message from ${server}.\n`);
});

httpServer.listen(port);

console.log(`The scale application is running on the server: ${server} and port: ${port}.`);
